var express = require("express");
const { Expo } = require("expo-server-sdk");
var router = express.Router();

let expo = new Expo();

router.post("/", function (req, res, next) {
  if (!req.body) {
    const msg = "no Pub/Sub message received";
    console.error(`error: ${msg}`);
    res.status(400).send(`Bad Request: ${msg}`);
    return;
  }
  if (!req.body.message) {
    const msg = "invalid Pub/Sub message format";
    console.error(`error: ${msg}`);
    res.status(400).send(`Bad Request: ${msg}`);
    return;
  }

  const pubSubMessage = req.body.message;
  const { pushTokens, sound, body, data } = pubSubMessage.data
    ? JSON.parse(Buffer.from(pubSubMessage.data, "base64").toString().trim())
    : {};

  if (
    pushTokens == null ||
    pushTokens === "undefined" ||
    pushTokens.length == 0
  ) {
    console.error("Impossible to send notifications");
    res.status(204).send();
    return;
  }

  const messages = prepareMessagesToSend(pushTokens, sound, body, data);

  sendNotifications(messages);

  console.log(`Notifications are sending to phones`);
  res.status(204).send();
});

const prepareMessagesToSend = (somePushTokens, sound, body, data) => {
  let messages = [];
  for (let pushToken of somePushTokens) {
    if (!Expo.isExpoPushToken(pushToken)) {
      console.error(`Push token ${pushToken} is not a valid Expo push token`);
      continue;
    }

    messages.push({
      to: pushToken,
      sound: sound != null && sound !== "" ? sound : "default",
      body,
      data,
    });
  }
  return messages;
};

const sendNotifications = async (messages) => {
  let chunks = expo.chunkPushNotifications(messages);
  let receiptIds = [];
  (async () => {
    let tickets = [];
    for (let chunk of chunks) {
      try {
        let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
        console.log(ticketChunk);
        tickets.push(...ticketChunk);
      } catch (error) {
        console.error(error);
      }
    }

    for (let ticket of tickets) {
      if (ticket.id) {
        receiptIds.push(ticket.id);
      }
    }
  })();
};

module.exports = router;
