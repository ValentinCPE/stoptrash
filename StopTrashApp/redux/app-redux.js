import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";

import * as firebase from "firebase";
import "firebase/firestore";

const ACTIONS = {
  SET_USER: "setUser",
  SET_TOKEN_NOTIF: "setTokenNotification",
};

// Initial State
const initialState = {
  user: {},
  tokenNotif: "",
};

// Reducer
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET_USER:
      return { ...state, user: action.value };
    case ACTIONS.SET_TOKEN_NOTIF:
      return { ...state, tokenNotif: action.value };
    default:
      return state;
  }
};

//Store
const store = createStore(reducer, applyMiddleware(thunkMiddleware));
export { store };

// Action Creators
const setUser = (user) => {
  return {
    type: ACTIONS.SET_USER,
    value: user,
  };
};

const setTokenNotif = (token) => {
  firebase.firestore().collection("users").doc(userId).set(
    {
      expoPushToken: token,
    },
    {
      merge: true,
    }
  );
  console.log(`Token ${token} for ${userId} has been updated in Firebase`);
  return {
    type: ACTIONS.SET_TOKEN_NOTIF,
    value: token,
  };
};

const userId = "deIkGvrfWBM33xFdnNEy";
const watchUser = () => {
  return function (dispatch) {
    firebase
      .firestore()
      .collection("users")
      .doc(userId)
      .onSnapshot(
        (doc) => {
          const user = doc.data();
          dispatch(setUser(user));
        },
        (err) => {}
      );
  };
};

export { setUser, watchUser, setTokenNotif };
