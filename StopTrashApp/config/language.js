import { Platform } from 'react-native';
import * as Localization from 'expo-localization';
import i18n from 'i18n-js';

import { fr } from './lang/fr';
import { en } from './lang/en';

const language = {};

language.init = async () => {
    if(Platform.OS === "android") {
        const { locale } = await Localization.getLocalizationAsync();
        i18n.locale = locale;
    } else {
        i18n.locale = await Localization.locale;
    }
    i18n.translations = {
        fr,
        en,
      };
      i18n.fallbacks = true;
}

language.t = (label) => {
    return i18n.t(label)
}

export const language;