import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { connect } from 'react-redux';
import { setUser, watchUser } from './../redux/app-redux';

const mapStateToProps = (state) => {
    return {
        user: state.user,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setUser: (user) => { dispatch(setUser(user)) },
        watchUser: () => { dispatch(watchUser()) },
    };
}

class MenuNavigatorScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user,
        };
        this.props.watchUser();
    }

    render() {
        const { user } = this.props;
        return(
            <View style={{paddingTop:20}}>
                <Text>{JSON.stringify(user)}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
});

export default connect(mapStateToProps, mapDispatchToProps)(MenuNavigatorScreen);