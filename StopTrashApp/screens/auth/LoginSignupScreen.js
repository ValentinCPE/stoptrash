import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Video } from "expo-av";

export default class LoginSignupScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>LoginSignup</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  mapStyle: {
    width: "100%",
    height: "100%",
  },
});
