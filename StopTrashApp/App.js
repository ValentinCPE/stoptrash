import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createAppContainer, createSwitchNavigator } from "react-navigation";

import * as firebase from "firebase";
import "firebase/firestore";
import { firebaseConfig } from "./constants/firebase";

import { Provider } from "react-redux";
import { store } from "./redux/app-redux";

import { registerForPushNotificationsAsync } from "./redux/notification";

import MapScreen from "./screens/MapScreen";
import SplashScreen from "./screens/SplashScreen";
import LoginSignupScreen from "./screens/auth/LoginSignupScreen";
import LoginScreen from "./screens/auth/LoginScreen";

import * as GoogleSignIn from "expo-google-sign-in";

export default class App extends React.Component {
  state = {
    notification: {},
  };

  constructor(props) {
    super(props);
    firebase.initializeApp(firebaseConfig);
    GoogleSignIn.initAsync();
  }

  componentDidMount() {
    registerForPushNotificationsAsync();
  }

  render() {
    return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    );
  }
}

const AppSwitchNavigator = createSwitchNavigator({
  SplashScreen: SplashScreen,
  LoginSignupScreen: LoginSignupScreen,
  LoginScreen: LoginScreen,
});

const AppNavigator = createAppContainer(AppSwitchNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
